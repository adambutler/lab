---
layout: post
title:   Meetup & conference video equipment
date:   2016-04-09 12:00:00
categories: articles
---

---

### Update: <small>*3rd September 2018*</small>
> You should totally go checkout [Recording talks at JSOxford meetups](https://marcusnoble.co.uk/2018-09-01-recording-talks-at-jsoxford-meetups/) by [Marcus Noble](https://twitter.com/Marcus_Noble) who has done an awesome job remixing this blog post and updating it with the equipment they use at [JSOxford](https://jsoxford.com/).

---

A little while ago we started getting over subscribed for the JavaScript group [Bristol JS](http://www.meetup.com/BristolJS/)
that I run. I wanted to give everybody who wanted the chance to watch the talks but we simply didn't have the capacity.


Filming the talks is a great way to give these people a chance to do so whilst opening our content out to the wider community.

We had some quite shoddy equipment to begin with which involved me filming the talks on my iPhone but when we started taking on sponsors this year we decided to give this equipment  an overhaul.


I was surprised to not find any information on what equipment was recommended. The closest thing I could find was [this blog post](http://blog.confreaks.net/2010/10/my-recording-equipment/) from back in 2010. I decided that once I got all our gear sorted I'd write about it.

So here it is, a comprehensive guide of the equipment we use and why we use it. If you have any questions you can email me on [adam@labfoo.dev](mailto:adam@labfoo.dev?Subject=Message from lab.io)

<img src="/img/features/video-equipment/flow.png" class="unstyled" />

## 1. Laptop

The setup allows for multiple screens to be connected and switched easily but is limited to devices with HDMI.
Digital outputs like display port can also be supported with [adapters](http://amzn.to/1UPMJby).
You can follow the dotted lines to see how all the equipment is connected.

## 2. HDMI Switch

The switch makes the handover to speakers seamless by pressing one button and does not interrupt video recording further down the line. We use the [Betron 3 Port HDMI Switch](http://amzn.to/1UPN7Hd).

## 3. HDMI Splitter

This is the secret sauce of having a good setup. Sometimes we go out to two screens making the splitter necessary without forcing the speaker to go into mirrored mode.

The splitter however serves an additional purpose in that it strips DRM meaning your captures won't fail in the middle of the talk. We use [this one](http://amzn.to/1Mm8e10) from Portta.

## 4. Capture Device

Software capture is hard to organise, inconsistent and can slow down a machine that is already having a hard time running two screens.

Hooking up a capture device also means you can get a single recording of the whole night making editing much easier.

The [mic](http://amzn.to/1S2Ee9u) simply captures the speaker without much background noise

## 5. Screen

You often don't have the control about where the content is displayed but if you do I'd highly recommend a TV for audiences up to 100 people and a high lumens projector for anything larger.

Most presenters will write there slides in 16:9 (1920 x 1080) these days so keep this in consideration and let your speakers know your setup early.

<br>

---

## Camcorder
You could just record the screen but body language says so much. I'd recommend investing in a low to mid range camcorder to capture your talks speakers.

The video quality should be good enough that it isn't distracting, battery life and quality of the microphone if far more important in my experience.

We use a [Sony HDRPJ810](http://amzn.to/1Mm8XPK). I picked up on an Amazon on a lightning deal for just £399. It has built in memory but we use a [high speed SD card](http://amzn.to/1Mm9m4I) so I don't have the take the camera with me when I'm editing.

## Syncing

Great now you've got the content in a few places, you might have noticed that the audio has been captured in two places this is for two reasons.
1. Synchronisation of the camcorder and capture device can be done automagically with the audio.
2. The capture device mic is great for picking up the speaker without background noise, but the camera mic is great capturing the audio during Q&A.

## Final Cut Pro X

Editing is made super easy with [Final Cut Pro X](https://itunes.apple.com/gb/app/final-cut-pro/id424389933?mt=12). Just make a multi-cam clip from your two sources, drop it into a project, open the multi-cam view and simply click the source you want shown whist watching back in real-time.

*Pro tip: If your tight for time you can do this at 2x speed using the timewarp feature.*

We use a [2TB external HDD](http://amzn.to/1Mm9SzL) since most of these projects reach at least 20GB.

## Equipment breakdown

| Item | Units | Cost <small>(at time of writing)</small> |
| [DisplayPort to HDMI Adapter](http://amzn.to/1UPMJby) | 1 | £5.99 |
| [Betron 3 Port HDMI Switch.](http://amzn.to/1UPN7Hd) | 1 | £6.99 |
| [Portta 2 Port 1x2 Hdmi Splitter](http://amzn.to/1Mm8e10) | 1 | £9.99 |
| [AVerMedia C875 - HD Capture](http://amzn.to/1UPSwOg) | 1 | £92.99 |
| [Sony HDRPJ810 Full HD Camcorder](http://amzn.to/1Mm8XPK) | 1 | £548.99 |
| [SanDisk Extreme 64 GB SDXC Class 10 Memory Card](http://amzn.to/1Mm9m4I) | 2 | £24.91 |
| [Seagate 2TB USB 3.0 HDD](http://amzn.to/1Mm9SzL) | 1 | £64.99 |
| [Final Cut Pro X.](https://itunes.apple.com/gb/app/final-cut-pro/id424389933?mt=12) | 1 | £229.99 |
| [Short HDMI leads](http://amzn.to/1UPR8eA) | 2 | £1.55 |
| [10ft HDMI leads](http://amzn.to/1MmauW6) | 2 | £6.49 |
| [Sony Ultra Compact Case for Handycam](http://amzn.to/1UPRou9) | 1 | £23.99 |
