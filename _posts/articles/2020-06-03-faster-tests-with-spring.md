---
layout: post
title: Faster RSpec tests with spring
date: 2020-06-03 00:00:00
categories: articles
overlay: '/img/features/rspec-spring/after.gif'
---

> TLDR; [Click here](#tldr) to jump to the end if you just want fast Ruby tests in VS Code.

I should have done this years ago, if only I knew.

I've been doing a lot more JavaScript recently, one of the things I've fallen in love with is the speed of testing and the interface provided in VS Code shown below.

![VS Code example running Jest](/img/features/rspec-spring/vscode-jest.gif)

I spend at least as much time if not more in Ruby and am a strong believer in optimising my workflow especially when it clearly pays dividends.

![Is It Worth the Time? XKCD comic](/img/features/rspec-spring/xkcd.png)

After a little bit of research, I find the [Ruby Test Explorer](https://marketplace.visualstudio.com/items?itemName=connorshea.vscode-ruby-test-adapter) extension for VS Code providing a similar UI to the one that [VS Code Jest](https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest) provides.

![Screenshot of Ruby Test Explorer](/img/features/rspec-spring/ruby-test-explorer.png)

I start using this and within a day become very frustrated about how slow it is and find myself reverting back to the terminal.

Today I found myself doing a bit of housekeeping and increasing test coverage in a few areas of [TechTalks](https://techtalks.io), that want for instant feedback returns so I investigate further.

### Running RSpec with Spring

How have I never considered this? Also, why is not the default?

Simply adding [spring-commands-rspec](https://github.com/jonleighton/spring-commands-rspec) reduced my single file tests from 4.5s to 0.4s a whopping 91.1% decrease in execution time.

<div id="tldr"></div>

### Setup

1. **Install VS Code [Ruby Test Explorer](https://marketplace.visualstudio.com/items?itemName=connorshea.vscode-ruby-test-adapter) extension.**

2. **Add the [spring-commands-rspec](https://github.com/jonleighton/spring-commands-rspec) gem to your project**

    ```ruby
    gem 'spring-commands-rspec', group: :development, require: false
    ```

3. **Generate the RSpec binstub**

    ```
    $ bundle exec spring binstub rspec
    ```

4. **Configure Ruby Test Explorer to use Spring.**

    Ensure the following is set in your workspace or user settings. Mine is within my project at `./.vscode/settings.json`.

    ```
    {
      "rubyTestExplorer.rspecCommand": "bundle exec spring rspec"
    }
    ```

### Result

Before | After
--- | ---
![GIF showing slow result before the change](/img/features/rspec-spring/before.gif) | ![GIF showing fast result after the change](/img/features/rspec-spring/after.gif)