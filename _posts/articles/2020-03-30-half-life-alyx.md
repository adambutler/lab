---
layout: post
title: A personal review of Half-Life Alyx
date: 2020-03-30 00:00:00
categories: articles
overlay: '/img/features/half-life-alyx/crab.gif'
---


This is a first for me.

Most of what I write is technical articles and documentation but I've been asked a lot recently about my thoughts on Half-Life Alyx. Although there are plenty of reviews of the game I found none that comes from the perspective that I have, a perspective that I'm sure is shared with many other people.

You see. I wouldn't call myself a gamer... at least not any more.

I grew up with Half-Life, I was 10 when the original game was released and played each subsequent edition on the day it was released. Through my university years, I would have called myself a gamer but in retrospect, a "benchmarker" might be more appropriate. I spent a lot of time trying to build a gaming PC that could run Crysis (my obsession at the time) consistently at 60 frames per second, this quickly became an expensive and unsustainable hobby.

In 2012 I eventually sold my gaming PC. I mostly lost interest in playing Xbox and slowly turned into a casual gamer, one that plays the odd half-hour of Mario Kart once or twice a month. I'd most certainly not call myself a gamer any more.

That was until Half-Life Alyx was announced. I was blown away by the trailer and immediately developed an itch that had been absent for several years.

I've always been somebody who loves the intersection between technology and art, particularly when the two play off each others strengths. I admire creators who harness this like Pixar who use some of the most advanced technologies that we have at our disposal to tell some of the richest and compelling stories of anybody in the industry. I think the Half-Life series has a seat at this table too.

When I think back to when the games were released they were all groundbreaking in their own way. There's an underlying exception that with each major release the bar is raised and our perceptions of what is possible are challenged and redefined.

<img src="/img/features/half-life-alyx/hl1.jpg" class="left feature" />

Half-Life (1998) was the first video game I experienced with a truly compelling linear story, until that point I'd played not much else beyond Quake and Doom. For the first time, the environments that I encountered had some meaningful context and the gameplay was led by a narrative which moved the game beyond just a point and shoot exercise to one that leads you through a diverse world and tells you a story that you're at the centre of. I never considered at the time the game to be a technical marvel, but the mechanics were like nothing I'd ever seen before and inspired many games that followed.

Half-Life 2 (2004) on the other hand was a technical marvel that still holds up 16 years later. Walking into City 17 and seeing the holographic screens at the time was exhilarating and had a lasting impact on me furthering my love of technology. The game had a groundbreaking physics engine that allowed for some really innovative puzzles that used the environment.

<img src="/img/features/half-life-alyx/hl2.jpg" class="right feature" />

I recently replayed some scenes ahead of the release of Half-Life Alyx. Revisiting games of my childhood usually leaves me wishing I had just preserved my rose-tinted glasses memory of them but in this case, the game was every bit as good as I remember it being all those years ago.

Both of these games helped establish an expectation that a major release of a Half-Life game should push the industry forward above and beyond our expectations. This goes a long way to explain the 13-year gap between the last edition and the even longer 16-year gap since the last major release. At any time Valve could have simply continued the series but truly living up to the Half-Life name would take a technological advance worthy of the name.

When the game was announced I was left with a dilemma, for several years now I've not had a gaming PC, let alone a VR headset. As time went by and I spoke with others about the game and got more excited about it I started to do a bit of research which only furthered my excitement. In the past, I had tried and had been incredibly impressed by the HTC Vive, as I started to look into the Valve Index specs I was really impressed at how much things had improved even over this short period. I took the leap with everything arriving just in time for release.

Was it all worth it. In short... Yes.

After 12 hours of gameplay, I can say Half-Life Alyx is nothing short of a masterpiece and everything a game of the series should be. Valve have played to the strengths of VR whilst developing elegant solutions for its limitations such as the ability to grab items from a distance using the gravity gloves. The story is compelling and is backed by some genuinely funny moments something I think the studio has excelled at since the release of Portal in 2007.

<img src="/img/features/half-life-alyx/world.gif" class="left feature" />

The environments are rich and interactive and is supported by VR by providing a better sense of scale and maintain tension and intimacy throughout many of the scenes throughout the sewers or the unusual alien-infested areas throughout the game.


It's evident that Valve has spent a lot of time and effort understanding what does and does not work well in VR. You can see evidence of this throughout their multitude of experiments in particular _The Lab_ which hosts a series of mini-games and experiences. I find working like this makes them very much akin to Pixar's experiments with their series of short movies.

All of this learning has led to them being able to include some amazing puzzles and elegant game mechanics such as the loading of the weapons that are truly innovative and make the most of the 3D space, hand tracking and feel incredibly natural.

<img src="/img/features/half-life-alyx/gun.gif" class="right feature" />

To say the least I'm very impressed. Buying the equipment was obviously a substantial investment but they had already earnt my respect over the last 22 years and I trusted them to do a great job. The game has achieved its goal of setting an example for what is possible and I'm positive given the overwhelmingly good reception that they and others will continue to take VR to its limitations.

Right now, I think VR is akin to where smartphones were in 2007. Much like Half-Life Alyx, the iPhone back then redefined our expectations, what followed was the refinements we've seen since and broader adoption of the technology. The bar has been set and I'm looking forward to seeing what they do next.

Long live VR.

---


### Equipment breakdown

I did _a lot_ of research when building this PC, I wanted something that would run Half-Life Alyx on optimal settings whilst still being cost sensitive.

I'm really happy with my setup. It runs everything super smoothly and is well balanced. Here is a breakdown of my rig for anybody who is interested:

| Item | Product | Cost <small>(at time of purchase)</small> |
| CPU | [AMD Ryzen 7 3700X](https://amzn.to/2QXMCO2) | £259.99 |
| Graphics | [NVIDIA GeForce RTX 2070](https://amzn.to/3dGIUlH) | £479.99 |
| RAM | [Corsair Vengeance LPX 32 GB](https://amzn.to/2QUHcn6) | £137.99 |
| SSD | [Crucial P1 500 GB NVMe/M.2 SDD](https://amzn.to/33WSyMN) | £64.79 |
| Motherboard | [MSI X570 GAMING PLUS Motherboard](https://amzn.to/3bEPO9n) | £158.98 |
| Case | [NZXT H510 - Compact ATX Case](https://amzn.to/344eRA7) | £69.98 |
| PSU | [Corsair CX550M 550W PSU](https://amzn.to/2QY0hVw) | £58.99 |
| VR Headset | [Valve Index](https://store.steampowered.com/valveindex) | £919.00 |
| | | **£2149.71** |
