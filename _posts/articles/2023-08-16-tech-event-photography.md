---
layout: post
title: Tech Event Photography
date: 2023-08-16 00:00:00
categories: articles
overlay: '/img/features/photography/photos/pixel-drone.jpg'
---

I'm often found at tech events taking photos, frequently mistaken for the official photographer. I'm not, I just like doing it and wanted to share why and some tips.

Anyone who knows me and my work well knows that I love the intersection between technology and creativity. Doing photography at tech events allows me to get to throw a bit of community into the mix too.

<img src="/img/features/photography/diagram.png" class="unstyled" />

## Technical, creative and community

I've always found conferences to be especially challenging as a photographer, especially whilst I'm also attending as a delegate.

Like in many creative endeavours, I enjoy working with the constraints and technical challenges the environment provides.

Here are some of the challenges I find are common:

- Venues are often dark, with harsh lighting.
- Speakers have slides which are projected far brighter than the surrounding environment.
- It's great to capture speakers whilst they are animated and gesturing - this is especially hard to capture well under low light conditions.
- It's often nice to capture the event's brand colours (spotlights) and logos in shot.
- Speakers are diverse and have a wide range of skin tones meaning that the same settings don't work for everyone.
- As a delegate, I'm usually in a fixed position and can't move around the room.
- Adjusting your shutter speed can create a rainbow effect on the screen - (sometimes this is desirable).

I tend to try to shoot, edit and publish photos during the talk itself. This is where community comes in. I've been involved in running tech events myself over the years and I've always enjoyed advocating for the rich, diverse and incredible tech communities I'm a part of. It helps build stronger ecosystems, encourages people along to share their knowledge and has helped me personally build connections in the industry.

I've actually found a personal benefit in doing this as well. Much like some people write notes to help them remember what they've learnt, I find that taking photos helps me to recall the content of the talk far beyond the day itself.

## Publishing

I usually post my photos up to 𝕏 (formally Twitter) towards the end of each session and will often comment on what I thought about the talk.

When posting I'll normally select one wide-angle shot (ideally with the title slide and event branding) and one profile shot of the speaker. I'll also try to include a link to the speaker's account and the event hashtag. Here is an example:

<blockquote class="twitter-tweet" data-dnt="true" data-theme="dark"><p lang="en" dir="ltr">CSS, JavaScript &amp; Accessibility by <a href="https://twitter.com/ireaderinokun?ref_src=twsrc%5Etfw">@ireaderinokun</a> at <a href="https://twitter.com/pixelpioneers?ref_src=twsrc%5Etfw">@pixelpioneers</a><br><br>Ire shares some fantasic practical tips on making the web more accessible to everybody.<a href="https://twitter.com/hashtag/PixelPioneers?src=hash&amp;ref_src=twsrc%5Etfw">#PixelPioneers</a> <a href="https://t.co/4Cv31vhOXO">pic.twitter.com/4Cv31vhOXO</a></p>&mdash; Adam Butler (@labfoo) <a href="https://twitter.com/labfoo/status/1669642164127641601?ref_src=twsrc%5Etfw">June 16, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

At the end of the event, I'll post a link to the full album on Flickr/Google Photos.

## Public domain

This is a personal choice but I post all my photos explicitly as public domain. This means that others including speakers can use them freely without attribution.

<img src="/img/features/photography/public-domain.svg" class="unstyled" />

## Some of my favourite photos

<div class="photo-grid">
  <div>
    <img src="/img/features/photography/photos/ana-balica-render.jpg" class="" />
    Ana Balica @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157682160349155/with/33657086072/">Render 2017</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/andy-nadia-bath-rb.jpg" class="" />
    Andy and Nadia @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157663528266334/with/25659621261/">Bath Ruby 2016</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/ben-foxall-render.jpg" class="" />
    Ben Foxall @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157682160349155/with/33657064512/">Render 2017</a>
  </div>
  
  <div>
    <img src="/img/features/photography/photos/james-white-fitc.jpg" class="" />
    James White @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157665098859416/with/25307137945/">FITC - Amsterdam 2016</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/cat-confconf.jpg" class="" />
    Cat Hunter @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157668591296156/with/27147564075/">ConfConf 2016</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/gav-josh-reasons.jpg" class="" />
    Joshua Davis and Gavin Strange @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157672526302392/with/28937548774/">Reasons to: Design, Code, Create 2016</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/gmunk-fitc.jpg" class="" />
    Gmunk @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157665098859416/with/25011683000/">FITC - Amsterdam 2016</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/ida-pixel.jpg" class="" />
    Ida @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157694984657382/with/29020800148/">Pixel Pioneers Workshop 2018</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/jessica-hische-reasons.jpg" class="" />
    Jessica Hische @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157672526302392/with/29562851795/">Reasons to: Design, Code, Create 2016</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/kent-beck-sw-ruby.jpg" class="" />
    Kent Beck @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157674082058988/with/44159966395/">Cookpad Sessions</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/jaycee-sw-ruby.jpeg" class="" />
    Jaycee @ <a href="https://photos.app.goo.gl/XEFoPybHVQ8obrfh7">South West Ruby - November 2019</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/matz-sw-ruby.jpg" class="" />
    Matz @ <a href="https://photos.app.goo.gl/4junhg8Gmd6e11e4A">South West Ruby - July 2019</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/liz-love.jpg" class="" />
    Liz Love @ <a href="https://photos.app.goo.gl/nS3rieTMexJbJi9z5">Product Tank - September 2018</a>
  </div>
  
  <div>
    <img src="/img/features/photography/photos/nic-alpi-dbm.jpg" class="" />
    Nic Alpi @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157698954934234/with/43591444851/">Design/ Build/ Market 2016</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/phil-nash-render.jpg" class="" />
    Phil Nash @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157682160349155/with/33657086072/">Render 2017</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/philhawksworth-pixels.jpg" class="" />
    Phil Hawksworth @ <a href="https://photos.app.goo.gl/VwGYM3FA7EYsbux16">Pixel Pioneers - 2023</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/pixel-drone.jpg" class="" />
    Drone shot @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157685799839256/with/35350803080/">Pixel Pioneers - 2017</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/remy-ffconf.jpg" class="" />
    Remy Sharp @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157701894261261/with/30862581487/">FF Conf 2018</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/oliver-confconf.jpg" class="" />
    Oliver Lindberg @ <a herf="https://www.flickr.com/photos/100289769@N08/albums/72157680877731274/with/34594967562/">Confconf 2017</a>
  </div>

  <div>
    <img src="/img/features/photography/photos/schwad-brighton-rb.jpg" class="" />
    Schwad @ <a href="https://www.flickr.com/photos/100289769@N08/albums/72157698954934234/with/43591444851/">Brighton Ruby 2023</a>
  </div>
</div>


## Advice

- Read and follow the events code-of-conduct.
- If in doubt check with the organisers before taking photos.
- Conferences often have different lanyards for people who don't want to be photographed.
- Limit the photos you take of delegates, leave that to the official photographers.
- People tend to avoid sitting in the front row, so you can get some good shots and won't distract people when editing.
- Be kind and respectful to others

## Equipment I use

Since I'm not a professional photographer I don't have a lot of high-end equipment, but I do have a few things that I use.

- Canon 77D
- Canon 50mm f/1.8 lens
- Canon EF 75-300mm f/4-5.6 III USM
- Peak Design Everyday Backpack 30L
- MacBook Pro 14" (2021)
