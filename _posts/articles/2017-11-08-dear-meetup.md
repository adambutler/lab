---
layout: post
title: Dear Meetup
date: 2017-11-08 00:00:00
categories: articles
---

Many of us, myself included, have spent years building communities on your platform. While not without rough edges, Meetup provides a suite of tools that have been an integral part of how we communicate with our members. We have been hugely encouraged by all the recent hard work that has been put in to improving the look and feel of Meetup after a long period of stagnation, but we feel that several of the changes will have a negative effect on our communities. In a few days you will force changes that impact how we communicate with our members and provide no ability to opt-out, removing important features and changing others in ways that will be detrimental to how we clearly communicate with our members. Individually, many are quite small, but the cumulative effect is significant.

Here are some of our concerns with the new version, which we hope you'll take as constructive feedback from long-time users of your platform.

![Google URL Shortener](/assets/meetup.png)

### Community page

- Only one organiser is listed by name, this introduces inequality for the many groups are equally run by several members.

### Details

- The copy is now cut-off meaning that the name, title and details of our second, third etc speakers are no longer shown by default.
- Text formatting has been removed, meaning that we can't implement headings for our speakers.
- Photos in the description are now loaded into a carousel and lose the context in which  they were placed.

### Organisers / Hosts / Attendees

- It's always useful to see who of your friends in the community are coming as well as the ones who are not, these are no longer exposed instead just opting for a select few.
- Organisers are no longer listed together at the top of the list.

### Comments

- Threading is completely broken, this has a negative effect for retrospective events.

### Sponsors

- Many communities rely on sponsorship, these changes drastically de-emphasise their presence.

### Diversity

The diversity of our attendees is something many of us work hard to address, especially for tech meetups where there is a preconception about what they will be like. a number of the changes will make that harder to address. Our Meetup event page is the shop window to our group. By hiding most of the attendees, and removing our ability to emphasise aspects of the group or event through simple formatting tools, it becomes much harder to encourage those who might have second thoughts by emphasising the positive steps we take.

### Wrap up

Much of the new interface is a definite improvement over the old version. The community page especially looks refreshed, in general, but we think your team need to spend a bit more time looking at how event hosts actually use the platform and build something that is compatible and empowers us to build strong communities with great planning and communication tooling.

❤️

[Adam Butler](http://twitter.com/labfoo) / [Bristol JS](https://meetup.com/BristolJS/)

[Roger Shepherd](http://twitter.com/RogerShepherd) / [Bristol and Bath Internet of Things Meetup](https://www.meetup.com/Bristol-and-Bath-Internet-of-Things-Meetup)

[Simon Starr](https://twitter.com/sstarr) / [Bath Ruby User Group](https://www.meetup.com/bathruby/)

[Lee Stone](https://twitter.com/leesto) / [PHPSW](https://www.meetup.com/php-sw/)

[Paul Leader](https://twitter.com/noneeeed) / [Bath Camp](https://www.meetup.com/BathCamp/)

[Nic Hemley](https://www.linkedin.com/in/nicholashemley/) / [BrisTech](https://www.meetup.com/bristech/)

[Nic Alpi](https://twitter.com/spyou) / [Design / Build / Market](https://www.meetup.com/Design-Build-Market)

**Are you a Meetup organiser and would like to sign this open letter? Add your details [here](https://adambutler.typeform.com/to/vOkfJt). These people have:**

[Karl Tynan](https://twitter.com/karltynan) & [Lee Kelleher](https://twitter.com/leekelleher) / [umBristol](https://www.meetup.com/umBristol/)

[Joseph Woodward](https://twitter.com/joe_mighty) / [.NET South West](https://www.meetup.com/dotnetsouthwest/)

[Dan Clarke](https://www.twitter.com/dracan) / [.NET Oxford](https://www.meetup.com/dotnetoxford)

[Andy Gale](https://twitter.com/techandygale) / [Bristol DevOps](https://www.meetup.com/Bristol-DevOps/)

[Mike Masey](http://twitter.com/MikeMasey) / [Front End Kent](https://www.meetup.com/Front-End-Kent/)

[Calum Ryan](https://twitter.com/calum_ryan) / [Tonbridge Digital](https://www.meetup.com/Tonbridge-Digital/)

[Mike Masey](http://twitter.com/MikeMasey) / [UmbKent](https://www.meetup.com/Kent-Umbraco-Meetup/)

[Jamie Middleton](https://twitter.com/jamiemid) / [TechSPARKuk](https://www.meetup.com/TechSPARKbristol)

Piers Storey / [Oxford Python](https://www.meetup.com/oxfordpython)

**Update 10th November 2017 08:15:**

😄 I'd like to thank Meetup CEO [Scott Heiferman](https://twitter.com/heif) & VP of Product [Fiona Spruill](https://twitter.com/fionaspruill) who have responded to this letter. Their responses acknowledge our concerns, and state that they take them seriously. Now we can only hope this translates into reality and changes are implemented to have a positive impact on our communities. See the responses below:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">We hear you, thx for caring so much! We’re setting up to make meetup MUCH better in 18. Will consider tweaks -don’t want to cause u trouble- but focused on bigsteps to help u make members happier,sponsors,diversity,etc. Thx for patience &amp;thoughful letter <a href="https://twitter.com/fionaspruill?ref_src=twsrc%5Etfw">@fionaspruill</a> <a href="https://twitter.com/farahassir?ref_src=twsrc%5Etfw">@farahassir</a></p>&mdash; Scott Heiferman (@heif) <a href="https://twitter.com/heif/status/928812298680102912?ref_src=twsrc%5Etfw">November 10, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<br>

<blockquote class="twitter-tweet" data-conversation="none" data-lang="en"><p lang="en" dir="ltr">Thanks for your thoughtful letter and for caring so much about <a href="https://twitter.com/Meetup?ref_src=twsrc%5Etfw">@Meetup</a>. We are listening and take your concerns seriously!</p>&mdash; Fiona Spruill (@fionaspruill) <a href="https://twitter.com/fionaspruill/status/928815526809305088?ref_src=twsrc%5Etfw">November 10, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
