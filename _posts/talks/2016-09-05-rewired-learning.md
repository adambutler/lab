---
layout: post
title:  Rewired Learning
title_long: "Rewired Learning <span class='subtle'>/ Reasons to: festival</span>"
date:   2016-09-05
categories: talks
slides: https://reasons.to/speakers/index.php?id=adambutler
event_name: Reasons to Festival
event_url: https://reasons.to
---

## Talk description

Coders have incredibly rewarding jobs, we get to build products that change the world and improve the quality of peoples' lives. However it is also one of the most mentally challenging careers - As developers we're expected to build something from nothing, that works across a increasingly diverse range of platforms in a constantly evolving landscape, all too often with very limited resource.

In this talk I'll cover these challenges as well as playful mechanics that you can use to tackle these issues, whilst developing yourself personally. We'll look at some strategies to ensure that you continue to test your assumptions, experiment often and stay motivated, including some fun hardware hacks that have taught me some genuine skills that I actually use in my job.

Expect to see Eugene, the IoT taxidermy ship it squirrel, mini missile launchers, drones and plenty of other stuff that are almost certainly applicable to building real shit, you know for banks and stuff.

## About the event

REASONS is a 3 day conference with a festival vibe, held annually in Brighton UK. Every year the very best international creative and developer speakers take to the stage to wow, inspire, inform, entertain, thrill, teach and mix with web designers and coders that attend from all over the world.

Find out more at [http://reasons.to](http://reasons.to)
