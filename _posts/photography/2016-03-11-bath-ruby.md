---
layout: post
date: 2016-03-11 00:00:00
categories: photography
title: Bath Ruby Conference
link: https://www.flickr.com/photos/100289769@N08/albums/72157663528266334
images:
  - https://farm2.staticflickr.com/1449/25754656395_6dec78e92f_z_d.jpg
  - https://farm2.staticflickr.com/1640/25728656826_45e323cdd4_z_d.jpg
  - https://farm2.staticflickr.com/1560/25659665491_9c349f20f1_z_d.jpg
  - https://farm2.staticflickr.com/1544/25124261354_82e52c860c_z_d.jpg
---
