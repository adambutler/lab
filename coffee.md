---
layout: page
title: Coffee
permalink: /coffee/
---

## Fancy grabbing a coffee sometime? It's on me.

We can chat about *anything* you want but here are some suggestions:

*Code, Art, Photography, Hardware Hacking, Mental Health, Tech Communities, Ordoo, Startups, Public Speaking, Drones, Portal, Back to the Future, Hardware, Software, The colour Purple and how it doesn't really exist, Chaos Theory, Tech Culture, IoT, Servers, The squirrel, Mailchimp Freddies, SaaS, PaaS, Retro arcade machines, Design, Personal Development, Steak, Books... or just about anything else.*

Just pick a date and time below:

<iframe
  src="https://calendly.com/labfoo/coffee"
  style="width:100%;
  height:650px;"
  frameBorder="0"
  scrolling="yes"
></iframe>
