---
layout: page
title: Projects
permalink: /projects/
---

{% include grid.html posts=site.categories.projects type='project' size=6 limit=100 %}
